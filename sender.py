import serial
from xbee import ZigBee

# Abrimos el puerto serie de la Raspberry para transmitir a 57600 baudios
serial_port = serial.Serial('/dev/ttyAMA0', 57600)

zb = ZigBee(serial_port)

# El sistema controlador debe estar siempre despierto escuchando peticiones
# que le puedan llegar de los sensores (no contemplado para Hackforgood)
while True:
    try:
        data = zb.wait_read_frame() # Get data for later use
        #print data # Dato recogido
        #print data['rf_data']

    except KeyboardInterrupt:
        break

serial_port.close()
